provider "openstack" {
  user_name = "eddie"
  tenant_name = "infra-ops"
  password = "REDACTED"
  auth_url = "http://controller-192-168-0-202.bropenstack.com:5000/v3"
  region = "RegionOne"
}

resource "openstack_compute_instance_v2" "hefe1" {
  name        = "hefe1.prod.sjc3"
  region    = "RegionOne"
  key_pair = "infra-ops-pub-key"
  image_id    = "14fc4a72-eee6-4893-ad3f-d6eccc84b158"
  flavor_id    = "86ff5992-dad4-4bd5-8a3c-77b5cfceffec"

  network {
    uuid = "b41a1d65-d339-40cf-b943-ef0b9ea547cb"
    name = "provider"
  }
}
resource "openstack_compute_instance_v2" "hefe2" {
  name        = "hefe2.prod.sjc3"
  region    = "RegionOne"
  key_pair = "infra-ops-pub-key" 
 image_id    = "14fc4a72-eee6-4893-ad3f-d6eccc84b158"
  flavor_id    = "86ff5992-dad4-4bd5-8a3c-77b5cfceffec"

  network {
    uuid = "b41a1d65-d339-40cf-b943-ef0b9ea547cb"
    name = "provider"
  }
}
resource "openstack_compute_instance_v2" "hefe3" {
  name        = "hefe3.prod.sjc3"
  region    = "RegionOne"
  key_pair = "infra-ops-pub-key" 
 image_id    = "14fc4a72-eee6-4893-ad3f-d6eccc84b158"
  flavor_id    = "86ff5992-dad4-4bd5-8a3c-77b5cfceffec"

  network {
    uuid = "b41a1d65-d339-40cf-b943-ef0b9ea547cb"
    name = "provider"
  }
}
resource "openstack_compute_instance_v2" "hefe4" {
  name        = "hefe4.prod.sjc3"
  region    = "RegionOne"
  image_id    = "14fc4a72-eee6-4893-ad3f-d6eccc84b158"
  key_pair = "infra-ops-pub-key" 
  flavor_id    = "86ff5992-dad4-4bd5-8a3c-77b5cfceffec"

  network {
    uuid = "b41a1d65-d339-40cf-b943-ef0b9ea547cb"
    name = "provider"
  }
}
resource "openstack_compute_instance_v2" "hefe5" {
  name        = "hefe5.prod.sjc3"
  region    = "RegionOne"
  key_pair = "infra-ops-pub-key" 
  image_id    = "14fc4a72-eee6-4893-ad3f-d6eccc84b158"
  flavor_id    = "86ff5992-dad4-4bd5-8a3c-77b5cfceffec"

  network {
    uuid = "b41a1d65-d339-40cf-b943-ef0b9ea547cb"
    name = "provider"
  }
}
